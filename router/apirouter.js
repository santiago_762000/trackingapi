var express = require('express');
var Customer = require('../modules/CustomerSchema.js');
var router = express.Router();



//To initialise the router configuration
router.use(function (req, res, next) {
 next();
});

//to create a general information message
router.get('/', function (req, res) {
 res.json({message: "This is an example to perform CRUD operations"});
});

router.route('/showAllCustomers')
    .get(function (req, res) {
        res.setHeader('Content-Type', 'text/html');
        Customer.find(function (err, customers) {
            if (err)
                res.send(err);
            res.json(customers);
        })

    });

router.route('/showCustomer/:customerId')
    .get(function (req, res) {
        res.setHeader('Content-Type', 'text/html');
        Customer.findById(req.params.customerId, function (err, customers) {
            if (err)
                res.send(err);
            res.json(customers);
        });

    });

router.route('/customerByEmail/:email')
    .get(function (req, res) {
        res.setHeader('Content-Type', 'text/html');
        Customer.findOne({'email': req.params.email}, function (err, customer) {
            if (err)
                res.send(err);
            res.json(customer);
        });

    });

router.route('/createCustomer')
    .post(function (req, res) {
        res.setHeader('Content-Type', 'text/html');
        var obj = new Customer();
        obj.name=req.body.name;
        obj.email=req.body.email;
        obj.date=req.body.date;
        obj.coordinates=[];
        obj.save(function (err) {
            if (err)
                res.send(err);
            res.json({message: 'Customer Created'});
        });
    });

router.route('/addCoordinate/:customerId')
    .put(function (req, res) {
        Customer.findById(req.params.customerId, function (err, customer) {
            if (err)
                res.send(err);

            customer.coordinates.push({
                "lat": req.body.lat,
                "lon": req.body.lon,
                "time": req.body.lon,
                "speed": req.body.speed
            });

            customer.save(function (err) {
                if (err)
                    res.send(err);

                res.json({message: 'Customer updated!'});
            });


        });
    });

router.route('/addCoordinateSpecialEvent/:customerId')
    .put(function (req, res) {
        Customer.findById(req.params.customerId, function(err, customer) {
            if (err)
                res.send(err);
            customer.coordinates.push({
                "lat": req.body.lat,
                "lon": req.body.lon,
                "time": req.body.lon,
                "speed": req.body.speed,
                "specialEvent": {
                    "nameEvent": req.body.nameEvent,
                    "typeEvent": req.body.typeEvent,
                    "place": req.body.place
                }
            });

            customer.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Customer updated!' });
            });
        })
    });


module.exports=router;
