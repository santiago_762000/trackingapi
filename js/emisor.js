var app=angular.module('EmisorApp',['ngWebsocket']);

app.controller('emisorController',function($scope, $websocket){
    $scope.lat='-27.4853201';
    $scope.lng='152.9905756';

    //var socket = io.connect('http://localhost:3000', {resource: 'nodejs'});
    var socket = io.connect('http://54.201.88.141:3000', {resource: 'nodejs'});
    $scope.createClient = function () {
        socket.emit('createClient', "{'name':'example123','email':'xyz@yahoo.com'");
    };
    
    $scope.sendMessage = function () {
        socket.emit('message', '{"lat":'+Number($scope.lat)+', "lng":'+Number($scope.lng)+', "speed":'+Number(133)+'}');
    };

    $scope.sendEvent = function () {
        socket.emit('event', '{"lat":'+Number($scope.lat)+', "lng":'+Number($scope.lng)+', "speed":'+Number(133)+',"nameEvent":"abc","typeEvent":"speed","place":"ghi"}');
    };
});




