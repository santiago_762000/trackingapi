var app=angular.module('TrackingApp',['leaflet-directive']);

app.config(function($logProvider){
    $logProvider.debugEnabled(false);
});

app.controller('mapController',['$scope',function($scope){
    //var socket = io.connect('http://localhost:3000', {resource: 'nodejs'});
    var socket = io.connect('http://54.201.88.141:3000', {resource: 'nodejs'});
    var points=[];
    $scope.center={};
    $scope.markers=[];
    $scope.currentPaths=[];
    socket.on('message', function(dataString){
    	var data=JSON.parse(dataString.replace(/'/g, '"'));
        points.push({ lat: data.lat, lng: data.lng });
        $scope.$apply(function(){
            $scope.messageReceived=data;

            $scope.center={
                lat: data.lat,
                lng: data.lng,
                zoom: 14
            }

            $scope.currentPaths=
                [{
                    color: 'purple',
                    weight: 4,
                    latlngs: points
                }
                ];


        })
    });
    socket.on('end',function(){
    console.log("done");
    });

    socket.on('event', function(data){

        $scope.$apply(function(){
            $scope.messageReceived=data;
            var dataObj=JSON.parse(data);
            var objMarker={
                lat: dataObj.lat,
                lng: dataObj.lng,
                message: dataObj.nameEvent,
                };

            var extraMarkerIcon;

            if(dataObj.typeEvent.toLowerCase()=="stop"){
                extraMarkerIcon= {
                    type: 'extraMarker',
                    icon: 'fa-hand-paper-o',
                    markerColor: 'red',
                    prefix: 'fa',
                    shape: 'circle'
                };
            }else if(dataObj.typeEvent.toLowerCase()=="warning"){
                extraMarkerIcon= {
                    type: 'extraMarker',
                    icon: 'fa-exclamation-triangle',
                    markerColor: 'yellow',
                    prefix: 'fa',
                    shape: 'circle'
                };
            }else if(dataObj.typeEvent.toLowerCase()=="speed"){
                extraMarkerIcon= {
                    type: 'extraMarker',
                    icon: 'fa-car',
                    markerColor: 'green',
                    prefix: 'fa',
                    shape: 'circle'
                };
            }
            else{
                extraMarkerIcon= {
                    type: 'extraMarker',
                    icon: 'fa-exclamation-triangle',
                    markerColor: '#f00',
                    prefix: 'fa',
                    shape: 'circle'
                };
            }


            objMarker.icon=extraMarkerIcon;
            /*objMarker.icon.markerColor='green';
            objMarker.icon.icon='fa-tag';*/
            $scope.markers.push(objMarker);

            $scope.center={
                lat: dataObj.lat,
                lng: dataObj.lng,
                zoom: 14
            }
        })
    });

    angular.extend($scope, {
        tiles: {
            name: 'Mapbox Park',
            url: 'https://api.mapbox.com/v4/{mapid}/{z}/{x}/{y}.{format}?access_token={apikey}',
            type: 'xyz',
            options: {
                apikey: 'pk.eyJ1Ijoic2FudGlhZ283NjIwMDAiLCJhIjoiY2lqbm5kbTAyMDBvd3V5bHhlY2IxZWFodSJ9.7NjgcoSQ0fvt51XgIhaJ4w',
                mapid: 'mapbox.streets',
                format:'png'
            }
        }
    });
}]);



