var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
    
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var router = require('./router/apirouter.js');
var crudOperations = require('./operations/crudoperations.js');
var obj;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/customerdb');

server.listen(3000);

app.get('/', function(req, res){
	crudOperations.findAll();
    //res.sendFile(__dirname + '/index.html');
});

app.get('/emisor', function(req, res){
    res.sendFile(__dirname + '/emisor.html');
});

app.get('/receptor', function(req, res){
    res.sendFile(__dirname + '/receptor.html');
});

io.sockets.on('connection', function(socket){
	socket.on('createClient', function(data){
		obj=crudOperations.createCustomer(data.name,data.email);
    });

    socket.on('message', function(data){
        io.sockets.emit('message', data);
        var dataObj=JSON.parse(data)
        crudOperations.addCoordinate(dataObj.lat,dataObj.lng,dataObj.speed,obj._id)
    });

    socket.on('event', function(data){
        io.sockets.emit('event', data);
        var dataObj=JSON.parse(data)
        crudOperations.addSpecialEvent(dataObj.lat,dataObj.lng,dataObj.speed,dataObj.nameEvent,dataObj.typeEvent,dataObj.place,obj._id);
    });
});