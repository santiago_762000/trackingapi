var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var Customer=new Schema({
 "name":String,
 "email":String,
 "date":{type: Date, default: Date.now},
 "coordinates": [
  {
   "lat": String,
   "lon": String,
   "time": {type: Date, default: Date.now},
   "speed": Number,
   "specialEvent": {
    "nameEvent": String,
    "typeEvent": String,
    "place":String
   }
  }
 ]
});
module.exports=mongoose.model('Customer',Customer);