var express = require('express');
var Customer = require('../modules/CustomerSchema.js');

function CrudOperations(){}

    CrudOperations.createCustomer=function(name,email){
    var obj = new Customer();
    obj.name=name;
    obj.email=email;
    obj.coordinates=[];
    obj.save(function (err) {
            if (err)
                console.log(err);
        });
    return obj;
}

CrudOperations.findAll=function(){
Customer.find(function (err, customers) {
   console.log(customers);

  })
}


CrudOperations.addCoordinate=function(lat,lon,speed,customerId){
    Customer.findById(customerId, function (err, customer) {
        if (err)
            res.send(err);

        customer.coordinates.push({
            "lat": lat,
            "lon": lon,
            "speed": speed
        });

        customer.save(function (err) {
            if (err)
                console.log(err);

        });
    });
}

CrudOperations.addSpecialEvent=function(lat,lon,speed,nameEvent,typeEvent,place,customerId){
    Customer.findById(customerId, function(err, customer) {
        if (err)
            res.send(err);
        customer.coordinates.push({
            "lat": lat,
            "lon": lon,
            "speed": speed,
            "specialEvent": {
                "nameEvent": nameEvent,
                "typeEvent": typeEvent,
                "place": place
            }
        });

        customer.save(function(err) {
            if (err)
                console.log(err);

        });
    })
}

module.exports=CrudOperations;